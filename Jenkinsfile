pipeline {
    agent any
    parameters {
        booleanParam(name: 'mp_on', defaultValue: true, description: 'Do you want to turn on Maintenance page?')
        choice(name: 'deploy_strategy', choices: ['rolling', 'concurrent'], description: 'rolling will deploy on admin-001 first and divide half of rest nodes into two patches; concurrent will deploy all nodes together')
        choice(name: 'var_node', choices: ['appall', 'app', 'adm'], description: 'Select nodes to be deployed')
        string(name: 'pkg_name', description: 'Put the package name here, e.g hybris_xyz_v02.06.zip')
        choice(name: 'buildType', choices: ['ant clean customize all', 'ant all'], description: 'Choose the build type')
        booleanParam(name: 'update_db', defaultValue: false, description: 'Do you need to update database, default is NO')
        string(name: 'db_json', description: 'Specify the db json file path, e.g /opt/hybris/config/config.json')
    }
    stages {
        stage ('Validation before deploy') {
            parallel {
                stage ('Validate package') {
                    steps {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            ansiblePlaybook colorized: true, extraVars: ['pkg_name':'${pkg_name}'], installation: 'ansible', playbook: '/opt/ansible/autodeployment/playbooks/validatePackge.yml', become: true
                        }
                    }
                }
                stage ('validate disk space on each host') {
                    steps {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            ansiblePlaybook colorized: true, forks: 2, installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/checkDisk.yml', become: true 
                        }
                    }    
                }
            }
        }
        stage ('Extrat package') {
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                		ansiblePlaybook colorized: true, extraVars: ['pkg_name':'${pkg_name}'], installation: 'ansible', playbook: '/opt/ansible/autodeployment/playbooks/extractPkg.yml', become: true 
                }
            }
        }
        stage ('Put maintenance page on') {
            steps {
                script {
                    if (mp_on == "true") {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            echo "Turn on maintenance page"
                            ansiblePlaybook colorized: true, forks: 2, installation: 'ansible', playbook: '/opt/ansible/autodeployment/playbooks/turnOnMP.yml', become: true
                        }
                    }else {
                        echo "MP will not turn on"
                    }
                }
            }
        }
        stage ('rolling: admin-001 deploy and start') {
        		when {
        				expression { params.deploy_strategy == 'rolling' }
        		}
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, extraVars: ['pkg_name':'${pkg_name}', 'buildType':'${buildType}', 'BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/deploy-adm-001.yml', become: true
                }
            }
        }
        stage ('rolling:  deploy reset nodes and start') {
        		when {
        				expression { params.deploy_strategy == 'rolling' }
        		}
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 5, extraVars: ['pkg_name':'${pkg_name}', 'buildType':'${buildType}', 'BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/deploy-rest.yml', become: true
                }
            }
        }
        stage ('rolling: shutting down all nodes if DB update') {
        		when {
        				expression { params.deploy_strategy == 'rolling' }
        		}
            steps {
                script {
                    if (update_db == "true") {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    				ansiblePlaybook colorized: true, forks: 5, installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/stopHybris.yml', become: true
                    				ansiblePlaybook colorized: true, forks: 5, extraVars: ['BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/archiveLog.yml', become: true
                        }                      
                    }else {
                        echo "No DB update"
                    }
                }
            }
        }        
        stage ('rolling: run DB update on admin-001') {
        		when {
        				expression { params.deploy_strategy == 'rolling' }
        		}
            steps {
                script {
                    if (update_db == "true") {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            ansiblePlaybook colorized: true, extraVars: ['db_json':'${db_json}','BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/updateDB.yml', become: true
                        }                      
                    }else {
                        echo "No DB update"
                    }
                }
            }
        }
        stage ('rolling: Start all hybris') {
            when {
        				expression { params.deploy_strategy == 'rolling' }
        		}
            steps {
            		script {
                    if (update_db == "true") {
                				wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    		ansiblePlaybook colorized: true, forks: 2, installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/startHybris.yml', become: true
                				}                     
                    }else {
                        echo "No DB update"
                   }
                }
            }
        }
       stage ('concurrent: Stop hybris') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}       
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 2, installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/stopHybris.yml', become: true
                }
            }
        }
        stage ('concurrent: Archive console log') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}           
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 5, extraVars: ['BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/archiveLog.yml', become: true
                }
            }
        }        
        stage ('concurrent: Deploy') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}           
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 2, extraVars: ['pkg_name':'${pkg_name}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/deploy.yml', become: true
                }
            }
        }
        stage ('concurrent: Build by ant') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}            
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 2, extraVars: ['buildType':'${buildType}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/buildApp.yml', become: true
                }
            }
        }
        stage ('concurrent: run DB update on admin-001') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}         
            steps {
                script {
                    if (update_db == "true") {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            ansiblePlaybook colorized: true, extraVars: ['db_json':'${db_json}','BUILD_NUMBER':'${BUILD_NUMBER}'], installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/updateDB.yml', become: true
                        }                      
                    }else {
                        echo "No DB update"
                    }
                }
            }
        }        
        stage ('concurrent: Start hybris') {
        		when {
        				expression { params.deploy_strategy == 'concurrent' }
        		}               
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    ansiblePlaybook colorized: true, forks: 2, installation: 'ansible', limit: '${var_node}', playbook: '/opt/ansible/autodeployment/playbooks/startHybris.yml', become: true
                }
            }
        }
        stage ('Turn maintenance page off') {
            steps {
                script {
                    if (mp_on == "true") {
                        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                            echo "Turn on maintenance page"
                            ansiblePlaybook colorized: true, forks: 5, installation: 'ansible', playbook: '/opt/ansible/autodeployment/playbooks/turnOffMP.yml', become: true
                        }
                    }else {
                        echo "MP will not turn on"
                    }
                }
            }
        }        
    }
}